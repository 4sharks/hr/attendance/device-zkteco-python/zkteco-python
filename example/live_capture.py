# -*- coding: utf-8 -*-
import os
import sys
import json
import send_data_to_api

CWD = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.dirname(CWD)
sys.path.append(ROOT_DIR)

from zk import ZK


conn = None
zk = ZK('172.16.150.98', port=4370)
try:
    conn = zk.connect()
    for attendance in conn.live_capture():
        if attendance is None:
            pass
        else:
            print (attendance)
            attend = str(attendance).split()
            type_split = str(attend[6]).replace(')','')
            type = int(type_split[0])
            print(type)
            if type == 0 :
                type = 'checkIn'
            elif type == 1 :
                type = 'checkOut'
            elif type == 2 :
                type = 'BreakOut'
            elif type == 3 :
                type = 'BreakIn'
            elif type == 4 :
                type = 'overtimeInv'
            elif type == 5 :
                type = 'overtimeOut'
            else:
                type = 'null'
            data = {
                        "emp_no" : attend[1].strip(),
                        "type" : type.strip(),
                        "attendance_date": attend[3].strip()+" "+ attend[4].strip(),
                        "provider_type" : "fingerprint",
                        "provider_device" : "fingerprint-machine-zk8000",
                        "tenant_id" : "658c29bd3e2e81dfcadf8a7d",
                        "company_id": "658c29bd3e2e81dfcadf8a7e"
                    }
            send_data_to_api.send(json.dumps(data))

except Exception as e:
    print ("Process terminate : {}".format(e))
finally:
    if conn:
        conn.disconnect()
